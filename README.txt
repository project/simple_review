
-- SUMMARY --

Allow users to review nodes using standard comments with an attached Fivestar
widget.

Compared to other, node-based review solutions (NodeReview, UserReview), this
module is an extremely light-weight solution (currently < 8 Kb).

For a full description visit the project page:
  http://drupal.org/project/simple_review
Bug reports, feature suggestions and latest developments:
  http://drupal.org/project/issues/simple_review


-- REQUIREMENTS --

* Voting API
  http://drupal.org/project/votingapi

* Fivestar
  http://drupal.org/project/fivestar


-- INSTALLATION --

1. Open simple_review.module with an editor and copy the stub function
   yourtheme_comment(). Switch to your theme directory, open template.php and
   add it to the end. Replace "yourtheme" in the function name with the name of
   your theme (ie. garland_comment(...) if you were using the Garland theme).
2. Make a backup of your theme's comment.tpl.php and copy the included
   comment.tpl.php to your theme directory.
3. Make any adjustments for your theme in comment.tpl.php.
4. This module only works with the comment form on a separate page. Verify the
   location of your comment submission form at Comments -> Settings
   (admin/content/comment/settings).
5. Enable reviews for your content types. Open the Content types page
   (admin/content/types) and edit a content type. Set the default comment
   setting to "Read/Write" and enable simple review functionality.
5. Go to a node that has reviews enabled. You should find a link beneath it
   that reads "Review this article" instead of the standard "Add a comment".


-- CONTACT --

Authors:
* Stefan M. Kudwien (smk-ka) - dev@unleashedmind.com
* Daniel F. Kudwien (sun) - dev@unleashedmind.com

This project has been sponsored by:
* UNLEASHED MIND
  Specialized in consulting and planning of Drupal powered sites, UNLEASHED
  MIND offers installation, development, theming, customization, and hosting
  to get you started. Visit http://www.unleashedmind.com for more information.

