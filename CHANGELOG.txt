
Simple Review 5.x-1.x, xxxx-xx-xx
---------------------------------
#212207: Added possibility to enable review functionality only for select node types.
#208297: Also deleting associated vote when a comment gets deleted.

Simple Review 5.x-1.0, 2007-01-08
---------------------------------
#174920 by sun: Removed 'new' marker on comment preview.

